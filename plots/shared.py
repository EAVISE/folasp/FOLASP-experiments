#!/usr/bin/python3

timeout = 5000
memout = 64000
mintime = 1
minmem = 100
maxground = 1000000000
minground = 100

rescol = 2
timecol1 = 3
timecol2 = 4
memcol = 5

families = {
    '01' : 'permutation_pattern_matching',
    '02' : 'valves_location_problem',
    '04' : 'still_life',
    '05' : 'graceful_graphs',
    '06' : 'bottle_filling',
    '07' : 'nomystery',
    '08' : 'sokoban',
    '09' : 'ricochet_robot',
    '10' : 'crossing_minimization',
    '11' : 'reachability',
    '12' : 'strategic_companies',
    '13' : 'solitaire',
    '14' : 'weighted_sequence',
    '15' : 'stablemarriage',
    '16' : 'incremental_scheduling',
    '17' : 'qualitative_spatial_reasoning',
    '18' : 'chemical_classification',
    '19' : 'abstract_dialectical_frameworks',
    '20' : 'visitall',
    '21' : 'complex_optimization',
    '22' : 'knight_tour',
    '23' : 'maximal_clique',
    '24' : 'labyrinth',
    '25' : 'minimal_diagnosis',
    '26' : 'hanoi_tower',
    '27' : 'graph_colouring',
    }

def convertFamily(family):
    for k,v in families.items():
        if k in family: return k+'_'+v
    return 'NO_FAMILY'

markers = [ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' ]

def getMarker(family):
    return '$'+markers[int(family[:2])]+'$'

colors = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'gold', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan', 'darkolivegreen', 'k', 'darksalmon']


def getColor(family):
    x = int(family[:2])
    x = 0 if x==14 else x
    x = 3 if x==15 else x
    x = 12 if x==16 else x
    assert(x<len(colors))
    return colors[x]

def isClingo(line):
    return line[0].startswith('clingo')
def isIDP(line):
    return line[0].startswith('/home/test/idp/build/bin/idp')
def isFolasp(line):
    return line[0].startswith('python3 /home/test/folasp/folasp.py')

def getFamily(line):
    if isClingo(line):
        return convertFamily(line[0].split('/')[5])
    elif isIDP(line):
        return convertFamily(line[0].split('/')[11])
    elif isFolasp(line):
        return convertFamily(line[0].split('/')[9])
    assert(False)

def success(line):
    if isFolasp(line):
        return int(line[rescol])==0 and _memFromLine(line)<memout and _timeFromLine(line)<timeout
    if isIDP(line):
        return int(line[rescol])==0 and _memFromLine(line)<memout and _timeFromLine(line)<timeout and len(line)>=9
    if isClingo(line):
        return int(line[rescol]) in {10,20,30} and _memFromLine(line)<memout and _timeFromLine(line)<timeout
    
def _timeFromLine(line):
    return max(float(line[timecol1])+float(line[timecol2]),mintime)

def timeFromLine(line):
    return _timeFromLine(line) if success(line) else timeout

def _memFromLine(line):
    return max(float(line[memcol])/1000,minmem)

def memFromLine(line):
    return _memFromLine(line) if success(line) else memout

def groundsizeFromLine(line):
    if isClingo(line):
        return max(minground,int(float(line[9]))) if len(line)>9 else maxground
    elif isIDP(line):
        return max(minground,int(float(line[6]))) if (len(line)>6 and len(line[6].strip())>0) else maxground
    elif isFolasp(line):
        return max(minground,int(float(line[8]))) if len(line)>8 else maxground
    assert(False)

