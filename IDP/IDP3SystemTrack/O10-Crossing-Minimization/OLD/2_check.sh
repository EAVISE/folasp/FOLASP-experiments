#!/bin/bash
# REMEMBER: first argument is a string representing a name for this run.
## timelimit: 0
## memlimit: 0
## output: |  | .check
## linked_input: |  | .out
## linked_input: | | .asp
## linked_input: |  | .out.TIM
line=`grep "Command exited with non-zero status " $4`
code=`echo $line | sed 's/[^0-9]//g'`
/home/jodv/workspace/asp-competition-2013/ASP/O10-Crossing-Minimization/checker/checker.sh $code $3 < $2
