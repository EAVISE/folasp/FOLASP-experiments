%

truck(T) :- fuel(T,_).
package(P) :- at(P,L), not truck(P).
location(L) :- fuelcost(_,L,_).
location(L) :- fuelcost(_,_,L).
locatable(O) :- at(O,L).
%
at(O,L,0) :- at(O,L).
fuel(T,F,0) :- fuel(T,F).
%

% unload/4, effects
at( P,L,S ) :- unload( P,T,L,S ).
del( in( P,T ),S ) :- unload( P,T,L,S ).

% load/4, effects
del( at( P,L ),S ) :- load( P,T,L,S ).
in( P,T,S ) :- load( P,T,L,S ).

% drive/4, effects
del( at( T,L1 ), S ) :- drive( T,L1,L2,S ).
at( T,L2,S ) :- drive( T,L1,L2,S). 
del( fuel( T,Fuelpre ),S ) :- drive( T,L1,L2,S ), fuel(T, Fuelpre,S-1).
fuel( T,Fuelpost,S ) :- drive( T,L1,L2,S ), fuelcost(Fueldelta,L1,L2), fuel(T,Fuelpre,S-1), Fuelpost = Fuelpre - Fueldelta.
% <<<<<  EFFECTS APPLY
% 
% INERTIA  >>>>>
at( O,L,S ) :- at( O,L,S-1 ), not del( at( O,L ),S  ), step(S).
in( P,T,S ) :- in( P,T,S-1 ), not del( in( P,T ),S  ), step(S).
fuel( T,Level,S ) :- fuel( T,Level,S-1 ), not del( fuel( T,Level) ,S ), truck( T ), step(S).
% <<<<<  INERTIA
% 

% 
% 
% PRECONDITIONS CHECK  >>>>>

% unload/4, preconditions
%u(P,T,L,S) :- unload( P,T,L,S ), not preconditions_u( P,T,L,S ).
 :- unload( P,T,L,S ), not preconditions_u( P,T,L,S ).
preconditions_u( P,T,L,S ) :- step(S), at( T,L,S-1 ), in( P,T,S-1 ), package( P ), truck( T ).

% load/4, preconditions
%l(P,T,L,S) :- load( P,T,L,S ), not preconditions_l( P,T,L,S ).
 :- load( P,T,L,S ), not preconditions_l( P,T,L,S ).
preconditions_l( P,T,L,S ) :- step(S), at( T,L,S-1 ), at( P,L,S-1 ).

% drive/5, preconditions
%d(T,L1,L2,S) :- drive( T,L1,L2,S ), not preconditions_d( T,L1,L2,S ).
 :- drive( T,L1,L2,S ), not preconditions_d( T,L1,L2,S ).
preconditions_d( T,L1,L2,S ) :- step(S), at( T,L1,S-1 ), fuel( T, Fuelpre, S-1), fuelcost(Fueldelta,L1,L2), Fuelpre - Fueldelta >= 0.
% <<<<<  PRECONDITIONS HOLD
% 

% GOAL CHECK

goalreached :- step(S),  N = #count{ at(P,L,S) : goal(P,L) }, N = #count{ goal(P,L) }.
:- not goalreached.

%#hide.
%#show u/4.
%#show l/4.
%#show d/4.
%#show goalreached/0.


