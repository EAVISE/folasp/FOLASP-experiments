vocabulary problemVoc {
  type Action
  type Cost isa int
  type Fuel isa int
  type Location
  type Object
  type Package isa Object
  type Step isa int
  type Truck isa Object
  AtGoalLocation(Package,Step)
  Blocked(Location,Step)
  Connected(Location,Location)
  DriveTo(Location,Step)
  Load(Package,Step)
  Loadable(Package,Step)
  Loaded(Package,Step)
  Unload(Package,Step)
  Unloadable(Package,Step)
  fuelcost(Cost,Location,Location)
    ActionAt(Step) : Action
    Drive : Action
    DropOff : Action
    FuelStart : Fuel
    GoalLocation(Package) : Location
    Nothing : Action
    PickUp : Action
    StartLocation(Package) : Location
    TruckAt(Step) : Location
    TruckStart : Location
}

theory T : problemVoc {
  (! P[Package] : AtGoalLocation(P,MAX[ : Step])).
(! STEP[Step] : ((ActionAt(STEP)  =  Drive) <=> (? LOC[Location] : DriveTo(LOC,STEP)))).
(! STEP[Step] : ((ActionAt(STEP)  =  PickUp) <=> (? PACKAGE[Package] : Load(PACKAGE,STEP)))).
(! STEP[Step] : ((ActionAt(STEP)  =  DropOff) <=> (? PACKAGE[Package] : Unload(PACKAGE,STEP)))).
(! STEP[Step] : ((ActionAt(STEP)  =  Nothing) <=> (! P[Package] : AtGoalLocation(P,STEP)))).
(ActionAt(MAX[ : Step])  =  Nothing).
(! STEP[Step] : (#{ LOC[Location] : DriveTo(LOC,STEP) }  =<  1)).
(! STEP[Step] : (#{ PACKAGE[Package] : Load(PACKAGE,STEP) }  =<  1)).
(! STEP[Step] : (#{ PACKAGE[Package] : Unload(PACKAGE,STEP) }  =<  1)).
(! P[Package] S[Step] : (~Load(P,S) | Loadable(P,S))).
(! P[Package] S[Step] : (~Unload(P,S) | Unloadable(P,S))).
(FuelStart  >=  sum{ C[Cost] LOC[Location] STEP[Step] : (DriveTo(LOC,STEP) & fuelcost(C,TruckAt(STEP),LOC)) : C }).
(! LOC[Location] STEP[Step] : (~DriveTo(LOC,STEP) | Connected(TruckAt(STEP),LOC))).
(! LOC[Location] STEP[Step] : (~Blocked(LOC,STEP) | ~DriveTo(LOC,STEP))).
(! STEP[Step] : (~(? PACKAGE[Package] : Loadable(PACKAGE,STEP)) | (ActionAt(STEP)  =  PickUp))).
(! STEP[Step] : (~((ActionAt(STEP)  ~=  PickUp) & (? PACKAGE[Package] : Unloadable(PACKAGE,STEP))) | (ActionAt(STEP)  =  DropOff))).
(! STEP[Step] : (~(Step(+(STEP,1)) & (ActionAt(STEP)  =  Nothing)) | (ActionAt(+(STEP,1))  =  Nothing))).
  {
    ! L1[Location] L2[Location] : Connected(L1,L2) <- (? C[Cost] : fuelcost(C,L1,L2)).
  }
  {
    TruckAt(MIN[ : Step])=TruckStart <- true.
    ! L[Location] STEP[Step] : TruckAt(+(STEP,1))=L <- ((TruckAt(STEP)  =  L) & (ActionAt(STEP)  ~=  Drive)).
    ! L[Location] STEP[Step] : TruckAt(+(STEP,1))=L <- DriveTo(L,STEP).
  }
  {
    ! PACKAGE[Package] : AtGoalLocation(PACKAGE,MIN[ : Step]) <- (GoalLocation(PACKAGE)  =  StartLocation(PACKAGE)).
    ! P[Package] S[Step] : AtGoalLocation(P,+(S,1)) <- AtGoalLocation(P,S).
    ! P[Package] S[Step] : AtGoalLocation(P,+(S,1)) <- Unload(P,S).
  }
  {
    ! P[Package] S[Step] : Loaded(P,+(S,1)) <- Load(P,S).
    ! P[Package] S[Step] : Loaded(P,+(S,1)) <- (Loaded(P,S) & ~Unload(P,S)).
  }
  {
    ! P[Package] S[Step] : Loadable(P,S) <- (~AtGoalLocation(P,S) & (~Loaded(P,S) & (TruckAt(S)  =  StartLocation(P)))).
    ! P[Package] S[Step] : Unloadable(P,S) <- ((GoalLocation(P)  =  TruckAt(S)) & Loaded(P,S)).
  }
  {
    ! L[Location] S[Step] : Blocked(L,+(S,1)) <- (TruckAt(S)  =  L).
    ! L[Location] S[Step] : Blocked(L,+(S,1)) <- (Blocked(L,S) & (ActionAt(S)  =  Drive)).
  }
}

structure S : problemVoc {
  Action = { "drive"; "load"; "nothing"; "unload" }
  Cost = { 1..1 }
  Fuel = { 56..56 }
  Location = { "a"; "b" }
  Object = { "p0"; "t0" }
  Package = { "p0" }
  Step = { 1..10 }
  Truck = { "t0" }
  fuelcost = { 1,"a","b"; 1,"b","a" }
  Drive = "drive"
  DropOff = "unload"
  FuelStart = 56
  GoalLocation = { "p0"->"b" }
  Nothing = "nothing"
  PickUp = "load"
  StartLocation = { "p0"->"a" }
  TruckStart = "a"
}

procedure main(){
printmodels(modelexpand(T,S))
}
