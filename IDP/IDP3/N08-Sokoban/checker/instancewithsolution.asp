player(player_01).
stone(stone_01).
isgoal(pos_03_04).
isnongoal(pos_03_05).
isnongoal(pos_03_06).
isnongoal(pos_03_07).
isnongoal(pos_03_03).
isnongoal(pos_01_01).
isnongoal(pos_01_02).
isnongoal(pos_01_03).
isnongoal(pos_01_04).
isnongoal(pos_01_05).
isnongoal(pos_01_06).
isnongoal(pos_01_07).
isnongoal(pos_01_08).
isnongoal(pos_01_09).
isnongoal(pos_01_10).
isnongoal(pos_02_01).
isnongoal(pos_02_02).
isnongoal(pos_02_03).
isnongoal(pos_02_04).
isnongoal(pos_02_05).
isnongoal(pos_02_06).
isnongoal(pos_02_07).
isnongoal(pos_02_08).
isnongoal(pos_02_09).
isnongoal(pos_02_10).
isnongoal(pos_03_01).
isnongoal(pos_03_02).
isnongoal(pos_03_08).
isnongoal(pos_03_09).
isnongoal(pos_03_10).
isnongoal(pos_04_01).
isnongoal(pos_04_02).
isnongoal(pos_04_03).
isnongoal(pos_04_04).
isnongoal(pos_04_05).
isnongoal(pos_04_06).
isnongoal(pos_04_07).
isnongoal(pos_04_08).
isnongoal(pos_04_09).
isnongoal(pos_04_10).
isnongoal(pos_05_01).
isnongoal(pos_05_02).
isnongoal(pos_05_03).
isnongoal(pos_05_04).
isnongoal(pos_05_05).
isnongoal(pos_05_06).
isnongoal(pos_05_07).
isnongoal(pos_05_08).
isnongoal(pos_05_09).
isnongoal(pos_05_10).
isnongoal(pos_06_01).
isnongoal(pos_06_02).
isnongoal(pos_06_03).
isnongoal(pos_06_04).
isnongoal(pos_06_05).
isnongoal(pos_06_06).
isnongoal(pos_06_07).
isnongoal(pos_06_08).
isnongoal(pos_06_09).
isnongoal(pos_06_10).
isnongoal(pos_07_01).
isnongoal(pos_07_02).
isnongoal(pos_07_03).
isnongoal(pos_07_04).
isnongoal(pos_07_05).
isnongoal(pos_07_06).
isnongoal(pos_07_07).
isnongoal(pos_07_08).
isnongoal(pos_07_09).
isnongoal(pos_07_10).
movedir(pos_01_01,pos_01_02,dir_down).
movedir(pos_01_01,pos_02_01,dir_right).
movedir(pos_01_02,pos_01_01,dir_up).
movedir(pos_01_02,pos_01_03,dir_down).
movedir(pos_01_03,pos_01_02,dir_up).
movedir(pos_01_03,pos_01_04,dir_down).
movedir(pos_01_04,pos_01_03,dir_up).
movedir(pos_01_04,pos_01_05,dir_down).
movedir(pos_01_05,pos_01_04,dir_up).
movedir(pos_01_05,pos_01_06,dir_down).
movedir(pos_01_06,pos_01_05,dir_up).
movedir(pos_01_06,pos_01_07,dir_down).
movedir(pos_01_07,pos_01_06,dir_up).
movedir(pos_01_07,pos_01_08,dir_down).
movedir(pos_01_08,pos_01_07,dir_up).
movedir(pos_01_08,pos_01_09,dir_down).
movedir(pos_01_09,pos_01_08,dir_up).
movedir(pos_01_09,pos_01_10,dir_down).
movedir(pos_01_10,pos_01_09,dir_up).
movedir(pos_01_10,pos_02_10,dir_right).
movedir(pos_02_01,pos_01_01,dir_left).
movedir(pos_02_10,pos_01_10,dir_left).
movedir(pos_03_03,pos_03_04,dir_down).
movedir(pos_03_03,pos_04_03,dir_right).
movedir(pos_03_04,pos_03_03,dir_up).
movedir(pos_03_04,pos_03_05,dir_down).
movedir(pos_03_04,pos_04_04,dir_right).
movedir(pos_03_05,pos_03_04,dir_up).
movedir(pos_03_05,pos_03_06,dir_down).
movedir(pos_03_05,pos_04_05,dir_right).
movedir(pos_03_06,pos_03_05,dir_up).
movedir(pos_03_06,pos_03_07,dir_down).
movedir(pos_03_06,pos_04_06,dir_right).
movedir(pos_03_07,pos_03_06,dir_up).
movedir(pos_03_07,pos_03_08,dir_down).
movedir(pos_03_07,pos_04_07,dir_right).
movedir(pos_03_08,pos_03_07,dir_up).
movedir(pos_03_08,pos_04_08,dir_right).
movedir(pos_04_02,pos_04_03,dir_down).
movedir(pos_04_02,pos_05_02,dir_right).
movedir(pos_04_03,pos_03_03,dir_left).
movedir(pos_04_03,pos_04_02,dir_up).
movedir(pos_04_03,pos_04_04,dir_down).
movedir(pos_04_03,pos_05_03,dir_right).
movedir(pos_04_04,pos_03_04,dir_left).
movedir(pos_04_04,pos_04_03,dir_up).
movedir(pos_04_04,pos_04_05,dir_down).
movedir(pos_04_04,pos_05_04,dir_right).
movedir(pos_04_05,pos_03_05,dir_left).
movedir(pos_04_05,pos_04_04,dir_up).
movedir(pos_04_05,pos_04_06,dir_down).
movedir(pos_04_05,pos_05_05,dir_right).
movedir(pos_04_06,pos_03_06,dir_left).
movedir(pos_04_06,pos_04_05,dir_up).
movedir(pos_04_06,pos_04_07,dir_down).
movedir(pos_04_06,pos_05_06,dir_right).
movedir(pos_04_07,pos_03_07,dir_left).
movedir(pos_04_07,pos_04_06,dir_up).
movedir(pos_04_07,pos_04_08,dir_down).
movedir(pos_04_07,pos_05_07,dir_right).
movedir(pos_04_08,pos_03_08,dir_left).
movedir(pos_04_08,pos_04_07,dir_up).
movedir(pos_04_08,pos_04_09,dir_down).
movedir(pos_04_08,pos_05_08,dir_right).
movedir(pos_04_09,pos_04_08,dir_up).
movedir(pos_04_09,pos_05_09,dir_right).
movedir(pos_05_02,pos_04_02,dir_left).
movedir(pos_05_02,pos_05_03,dir_down).
movedir(pos_05_03,pos_04_03,dir_left).
movedir(pos_05_03,pos_05_02,dir_up).
movedir(pos_05_03,pos_05_04,dir_down).
movedir(pos_05_04,pos_04_04,dir_left).
movedir(pos_05_04,pos_05_03,dir_up).
movedir(pos_05_04,pos_05_05,dir_down).
movedir(pos_05_05,pos_04_05,dir_left).
movedir(pos_05_05,pos_05_04,dir_up).
movedir(pos_05_05,pos_05_06,dir_down).
movedir(pos_05_06,pos_04_06,dir_left).
movedir(pos_05_06,pos_05_05,dir_up).
movedir(pos_05_06,pos_05_07,dir_down).
movedir(pos_05_07,pos_04_07,dir_left).
movedir(pos_05_07,pos_05_06,dir_up).
movedir(pos_05_07,pos_05_08,dir_down).
movedir(pos_05_08,pos_04_08,dir_left).
movedir(pos_05_08,pos_05_07,dir_up).
movedir(pos_05_08,pos_05_09,dir_down).
movedir(pos_05_08,pos_06_08,dir_right).
movedir(pos_05_09,pos_04_09,dir_left).
movedir(pos_05_09,pos_05_08,dir_up).
movedir(pos_05_09,pos_06_09,dir_right).
movedir(pos_06_08,pos_05_08,dir_left).
movedir(pos_06_08,pos_06_09,dir_down).
movedir(pos_06_09,pos_05_09,dir_left).
movedir(pos_06_09,pos_06_08,dir_up).
movedir(pos_07_01,pos_07_02,dir_down).
movedir(pos_07_02,pos_07_01,dir_up).
movedir(pos_07_02,pos_07_03,dir_down).
movedir(pos_07_03,pos_07_02,dir_up).
movedir(pos_07_03,pos_07_04,dir_down).
movedir(pos_07_04,pos_07_03,dir_up).
movedir(pos_07_04,pos_07_05,dir_down).
movedir(pos_07_05,pos_07_04,dir_up).
movedir(pos_07_05,pos_07_06,dir_down).
movedir(pos_07_06,pos_07_05,dir_up).
at(player_01,pos_06_08).
at(stone_01,pos_05_03).
clear(pos_04_04).
clear(pos_04_05).
clear(pos_04_06).
clear(pos_05_07).
clear(pos_01_01).
clear(pos_01_02).
clear(pos_01_03).
clear(pos_01_04).
clear(pos_01_05).
clear(pos_01_06).
clear(pos_01_07).
clear(pos_01_08).
clear(pos_01_09).
clear(pos_01_10).
clear(pos_02_01).
clear(pos_02_10).
clear(pos_03_03).
clear(pos_03_04).
clear(pos_03_05).
clear(pos_03_06).
clear(pos_03_07).
clear(pos_03_08).
clear(pos_04_02).
clear(pos_04_03).
clear(pos_04_07).
clear(pos_04_08).
clear(pos_04_09).
clear(pos_05_02).
clear(pos_05_04).
clear(pos_05_05).
clear(pos_05_06).
clear(pos_05_08).
clear(pos_05_09).
clear(pos_06_09).
clear(pos_07_01).
clear(pos_07_02).
clear(pos_07_03).
clear(pos_07_04).
clear(pos_07_05).
clear(pos_07_06).
goal(stone_01).
step(1).
step(2).
step(3).
step(4).
step(5).
step(6).
step(7).
step(8).
step(9).
step(10).
step(11).
step(12).
step(13).
step(14).
step(15).
step(16).
step(17).
step(18).
step(19).
step(20).
step(21).
step(22).
step(23).
step(24).
step(25).
step(26).
step(27).
step(28).
step(29).
step(30).
pushtogoal(player_01,stone_01,pos_03_06,pos_03_05,pos_03_04,dir_up,30). pushtonongoal(player_01,stone_01,pos_05_05,pos_04_05,pos_03_05,dir_left,29). pushtonongoal(player_01,stone_01,pos_05_05,pos_04_05,pos_03_05,dir_left,28). pushtonongoal(player_01,stone_01,pos_04_07,pos_04_06,pos_04_05,dir_up,27). pushtonongoal(player_01,stone_01,pos_04_08,pos_04_07,pos_04_06,dir_up,26). pushtonongoal(player_01,stone_01,pos_04_09,pos_04_08,pos_04_07,dir_up,25). pushtonongoal(player_01,stone_01,pos_06_08,pos_05_08,pos_04_08,dir_left,24). pushtonongoal(player_01,stone_01,pos_05_06,pos_05_07,pos_05_08,dir_down,23). move(player_01,pos_04_08,pos_03_08,dir_left,22). move(player_01,pos_04_08,pos_04_09,dir_down,21). pushtonongoal(player_01,stone_01,pos_05_05,pos_05_06,pos_05_07,dir_down,20). pushtonongoal(player_01,stone_01,pos_05_04,pos_05_05,pos_05_06,dir_down,19). pushtonongoal(player_01,stone_01,pos_05_03,pos_05_04,pos_05_05,dir_down,18). pushtonongoal(player_01,stone_01,pos_05_02,pos_05_03,pos_05_04,dir_down,17). pushtonongoal(player_01,stone_01,pos_05_02,pos_05_03,pos_05_04,dir_down,16). pushtonongoal(player_01,stone_01,pos_05_02,pos_05_03,pos_05_04,dir_down,15). move(player_01,pos_05_03,pos_05_02,dir_up,14). move(player_01,pos_05_03,pos_04_03,dir_left,13). move(player_01,pos_03_06,pos_03_05,dir_up,12). pushtonongoal(player_01,stone_01,pos_05_04,pos_05_03,pos_05_02,dir_up,11). move(player_01,pos_05_05,pos_05_04,dir_up,10). move(player_01,pos_05_06,pos_05_05,dir_up,9). move(player_01,pos_05_07,pos_05_06,dir_up,8). move(player_01,pos_03_08,pos_03_07,dir_up,7). move(player_01,pos_03_07,pos_03_06,dir_up,6). move(player_01,pos_05_08,pos_05_07,dir_up,5). move(player_01,pos_03_08,pos_03_07,dir_up,4). move(player_01,pos_04_08,pos_03_08,dir_left,3). move(player_01,pos_05_08,pos_04_08,dir_left,2). move(player_01,pos_06_08,pos_05_08,dir_left,1).
