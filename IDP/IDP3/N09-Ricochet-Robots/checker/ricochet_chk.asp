#const row =  1.
#const col = -1.

% helper predicates
dir(west, -1, 0).
dir(east,  1, 0).
dir(north, 0,-1).
dir(south, 0, 1).

dl(west;north,-1).
dl(east;south, 1).

dir(west;east,row).
dir(north;south,col).

dir(D) :- dir(D,_).

robot(R) :- pos(R,_,_).

% initial position of the robots
pos(R,row,I,0) :- pos(R,I,_).
pos(R,col,J,0) :- pos(R,_,J).

% blockages (just to be sure)
barrier(I+1,J,west ) :- barrier(I,J,east ), dim(I;J;I+1).
barrier(I,J+1,north) :- barrier(I,J,south), dim(I;J;J+1).
barrier(I-1,J,east ) :- barrier(I,J,west ), dim(I;J;I-1).
barrier(I,J-1,south) :- barrier(I,J,north), dim(I;J;I-1).

% connections
conn(D,I,J) :- dir(D,col), dir(D,_,DJ), not barrier(I,J,D), dim(I;J;J+DJ).
conn(D,J,I) :- dir(D,row), dir(D,DI,_), not barrier(I,J,D), dim(I;J;I+DI).

% #cumulative t.

step(1..X) :- length(X).
 :- go(_,_,_,T), not step(T).
 :- step(T), not 1 { go(_,_,_,T) } 1.

% plan generator
% 1 { selectRobot(R,t) : robot(R) } 1.
selectRobot(R,T) :- go(R,D,O,T).
 :- selectRobot(R,_), not robot(R).

%%1 { selectDir(D,O,t) : dir(D,O) } 1.
selectDir(D,O,T) :- go(R,D,O,T).
 :- selectDir(D,O,_), not dir(D,O).

%%go(R,D,O,t) :- selectRobot(R,t), selectDir(D,O,t).
go_(R,O,T)   :- go(R,_,O,T).
 % cleaned-up output
go(R,west,1,T) :- go(R,west,T).
go(R,east,1,T) :- go(R,east,T).
go(R,south,-1,T) :- go(R,south,T).
go(R,north,-1,T) :- go(R,north,T).
%:- go(R,D,T), not go(R,D,O,T).

% no crossing of blockages or robots
sameLine(R,D,O,RR,T)  :- go(R,D,O,T), pos(R,-O,L,T-1), pos(RR,-O,L,T-1), R != RR.
blocked(R,D,O,I+DI,T) :- go(R,D,O,T), pos(R,-O,L,T-1), not conn(D,L,I), dl(D,DI), dim(I;I+DI).
blocked(R,D,O,L,T)    :- sameLine(R,D,O,RR,T), pos(RR,O,L,T-1).

% which places are reachable by a robot
reachable(R,D,O,I,   T) :- go(R,D,O,T), pos(R,O,I,T-1).
reachable(R,D,O,I+DI,T) :- reachable(R,D,O,I,T), not blocked(R,D,O,I+DI,T), dl(D,DI), dim(I+DI).

% one has to be moved
% :- go(R,D,O,T), pos(R,O,I,T-1), blocked(R,D,O,I+DI,T), dl(D,DI).
% :- go(R,D,O,T), go(R,DD,O,T-1).

% move the robot
pos(R,O,I,T) :- reachable(R,D,O,I,T), not reachable(R,D,O,I+DI,T), dl(D,DI).
pos(R,O,I,T) :- pos(R,O,I,T-1), not go_(R,O,T), step(T).

% redundant (important for harder ones)
% :- not 1 { pos(R,row,I,t) } 1, robot(R).
% :- not 1 { pos(R,col,I,t) } 1, robot(R).
% selectDir(O,t) :- selectDir(D,O,t).
% :- selectDir(O,t), robot(R), pos(R,-O,I,t), not pos(R,-O,I,t-1).

% #volatile t.

:- target(R,I,_), not pos(R,row,I,X), length(X).
:- target(R,_,J), not pos(R,col,J,X), length(X).

#hide.
#show go/4.
% just for visualization
%#show pos/4.
%#show conn/3.
%#show target/3.

#base.
