% Check whether the subsequence is a matching
:- solution(K1,ET1), solution(K2,ET2), p(K1,EP1), p(K2,EP2), ET1 < ET2, EP1 >= EP2.

% Check whether the solution contains exactly the indices 1,...,patternlength 
pind_(I) :- p(I,_).
solind_(I) :- solution(I,_).
:- solind_(I), not pind_(I).
:- pind_(I), not solind_(I).

% Only existing elements are in the solution
tind_(I) :- t(I,_).
:- solution(_,E), not tind_(E).

% No element of the text t is chosen more than once
:- solution(K1,E), solution(K2,E), K1 != K2.

% Check whether the order of the chosen elements is the same as in the text t
esol_(K,I,E) :- solution(K,E), t(I,E).
:- esol_(K1,I1,_), esol_(K2,I2,_), K1<K2, I1 >= I2.
