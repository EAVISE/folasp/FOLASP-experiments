#! /bin/bash
ok=false

read answerRow
MYPATH=$(dirname $0)

if [ "$answerRow" == "INCONSISTENT" ] && [ $1 != 20 ]; then
	echo "FAIL"
	echo "MISMATCH OF SOLVER RETURN VALUE AND STANDARD INPUT" >&2
	exit 1
fi
if [ "$answerRow" != "INCONSISTENT" ] && [ $1 == 20 ]; then
	echo "FAIL"
	echo "MISMATCH OF SOLVER RETURN VALUE AND STANDARD INPUT" >&2
	exit 1
fi
if [ "$answerRow" != "INCOMPLETE" ] && [ $1 != 10 ] && [ $1 != 20 ]; then
	echo "FAIL"
	echo "MISMATCH OF SOLVER RETURN VALUE AND STANDARD INPUT" >&2
	exit 1
fi

if [ $1 == 20 ]; then
	echo "DONTKNOW"
        echo "Instances with no solution should not be checked with this checker" >&2
        exit 2
else
	if [ $1 != 10 ]; then
		echo "FAIL"
	        exit 1
	else
		# Filter for output predicates col/2, everything else will be ignored
		filteredAnswer=$(echo "$answerRow" | grep -o -e 'solution([^,^)]\+,[^)^,]\+)\.')
		# Provide filtered witness, additionally specify input instance ($2)
		systemResult=$(echo "$filteredAnswer" | "$MYPATH"/dl.bin -silent "$MYPATH"/constraint.asp $2 --)
		if [[ $? -ne 0 ]]
		then
    			echo "WARN"
    			echo "Unrecognized input stream or general checker failure." >&2
    			exit 1
		fi

		if [ "$systemResult" == "" ]; then
			ok=false
		else
			ok=true
		fi
	fi
fi
if $ok; then
	echo "OK"
else
	echo "FAIL"
	exit 1
fi 
