#!/usr/bin/env python

import sys

if len(sys.argv) != 1 or "-h" in sys.argv or "--help" in sys.argv:
    print "Usage: ./checker < {witness file}"
    sys.exit(0)

assert len(sys.argv) == 1


def readpred(pred):
    pred = pred.strip()
    if pred.endswith('\n'):
        pred = pred[:-1]

    assert pred.endswith(')')

    psp = pred[:-1].split('(')
    
    assert len(psp) == 2

    ftor = psp[0]
    args = [ x.strip() for x in psp[1].split(',') ]

    return (ftor, args)

#fgr = open(sys.argv[1],'r')

nlayers = None
edges = []

width = {}
layermap = {}
in_layer = {}
posmap = {}

fail = False


## Read the original problem
#for line in fgr.readlines():
for line in sys.stdin.readlines():
    for pred in line.strip().split('.'):
        if not pred.endswith(')'):
            continue

        ftor, args = readpred(pred)

        if(ftor == 'layers'):
            assert len(args) == 1
            nlayers = int(args[0])

        if(ftor == 'width'):
            assert len(args) == 2
            width[int(args[0])] = int(args[1])
        
        if(ftor == 'edge'):
            assert len(args) == 2
            edges.append( (args[0],args[1]) )
        if(ftor == 'in_layer'):
            assert len(args) == 2
            l = int(args[0])
            layermap[args[1]] = l
            if l not in in_layer:
                in_layer[l] = []
            in_layer[l].append(args[1])

        if(ftor == 'position'):
            assert len(args) == 2
            if args[0] in posmap.keys():
                fail = True
            posmap[args[0]] = int(args[1])

#print layermap
for l in layermap.keys():
    if l not in posmap.keys():
        fail = True
        break
    if posmap[l] > width[layermap[l]]:
        fail = True
        break
    for n in in_layer[layermap[l]]:
	if not n in posmap.keys():
	    fail = True
	    break
        if n != l and posmap[n] == posmap[l]:
            fail = True
            break

if fail:
    print "FAIL"
    sys.exit(20)	
else:
    # Count the number of crossings.
    crossings = 0
    for i in range(0, len(edges)-1):
        e0 = edges[i]

        for j in range(i+1,len(edges)):
            e1 = edges[j]
            if layermap[ e0[0] ] != layermap[ e1[0] ] or e0[0] == e1[0] or e0[1] == e1[1]:
                continue

            assert layermap[ e0[1] ] == layermap[ e1[1] ]
            if e0[0] not in posmap or e0[1] not in posmap or e1[0] not in posmap or e1[1] not in posmap:
                print "FAIL"
                sys.exit(20)
            if posmap[ e0[0] ] == posmap[ e1[0] ] or posmap[ e0[1] ] == posmap[ e1[1] ]:
                print "FAIL"
                sys.exit(20)

            if( (posmap[ e0[0] ] < posmap[ e1[0] ]) != (posmap[ e0[1] ] < posmap[ e1[1] ]) ):
                crossings += 1

    #if len(sys.argv) == 4:
    #if len(sys.argv) == 2:
    #    optfile = open(sys.argv[3],'r')
    #    optfile = open(sys.argv[1],'r')
    #    if crossings == int(optfile.readline()):
    #        print "Correct objective value."
    #    else:
    #        print "Error: Incorrect objective."
    #    optfile.close()
    #else:

    print "OK", crossings
    sys.exit(10)
