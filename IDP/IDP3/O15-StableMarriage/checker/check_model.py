#!/usr/bin/env python

import sys
import ply.yacc as yacc

from lex import tokens

inv = {}

manAssignsScore = {}
womanAssignsScore = {}
match = {}
matchReverse = {}

def p_atoms(p):
    """atoms : """

def p_atoms_rec(p):
    """atoms : atoms atom DOT"""    

def p_atom(p):
    """atom : ID LPAREN terms RPAREN"""
    if p[1] == "manAssignsScore":
        if not p[3][0] in manAssignsScore.keys():
            manAssignsScore[p[3][0]] = {}
        manAssignsScore[p[3][0]][p[3][1]] = p[3][2]
    elif p[1] == "womanAssignsScore":
        if not p[3][0] in womanAssignsScore.keys():
            womanAssignsScore[p[3][0]] = {}
        womanAssignsScore[p[3][0]][p[3][1]] = p[3][2]
    elif p[1] == "match":
        if p[3][0] in match.keys():
            inv[0] = 0
        match[p[3][0]] = p[3][1]
        
        if p[3][1] in matchReverse.keys():
            inv[0] = 0
        matchReverse[p[3][1]] = p[3][0]
        

def p_terms_term(p):
    """terms : term"""
    p[0] = [p[1]]

def p_terms_rec(p):
    """terms : terms COMMA term"""
    p[0] = p[1]
    p[0].append(p[3])
    
def p_term_id(p):
    """term : ID"""
    p[0] = p[1]
    
def p_term_number(p):
    """term : NUMBER"""
    p[0] = p[1]
    
        

# Error rule for syntax errors
def p_error(p):
    print "WARN"
    sys.exit(1)

# Build the parser
parser = yacc.yacc()

while True:
   try:
       s = raw_input()
   except EOFError:
       break
   if not s: continue
   result = parser.parse(s)

def check():
    if len(match) != len(manAssignsScore):
        return True
        
    for m in manAssignsScore.keys():
        if not m in match.keys():
            return True

        for w in womanAssignsScore.keys():
            if not w in match.values():
                return True
                
            if not match[m] == w:
                w1 = match[m]
                s = manAssignsScore[m][w]
                s1 = manAssignsScore[m][w1]
                if s > s1:
                    m1 = matchReverse[w]
                    s = womanAssignsScore[w][m]
                    s1 = womanAssignsScore[w][m1]
                    if s >= s1:
                        return True
    return False

if 0 in inv.keys() or check():
    print "FAIL"
else:
    print "OK"
                    
