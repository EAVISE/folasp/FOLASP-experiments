import ply.lex as lex

tokens = (
    'NUMBER',
    'ID',
    'LPAREN',
    'RPAREN',
    'DOT',
    'COMMA',
)

t_ignore = " \t\r"

t_LPAREN    = r'\('
t_RPAREN    = r'\)'
t_DOT       = r'\.'
t_COMMA     = r','

def t_ignore_COMMENT(t):
    r'\%.*'
    t.lexer.skip(1)
    
def t_NUMBER(t):
    r'\d+'
    t.value = int(t.value)
    return t
    
def t_ID(t):
    r'\w[\w\d_]*'
    return t

def t_newline(t):
    r'\n'
    t.lexer.lineno += len(t.value)

def t_error(t):
    print "Syntax error on '%s' in line %d" % (t.value[0], t.lexer.lineno,)
    t.lexer.skip(1)
    
lexer = lex.lex()

