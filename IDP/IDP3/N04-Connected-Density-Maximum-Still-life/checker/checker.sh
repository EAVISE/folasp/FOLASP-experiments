#! /bin/bash
read answerRow

if [ "$answerRow" == "INCONSISTENT" ] && [ $1 != 20 ]; then
	echo "FAIL"
#	echo "MISMATCH OF SOLVER RETURN VALUE AND STANDARD INPUT"
	exit 1
fi
if [ "$answerRow" != "INCONSISTENT" ] && [ $1 == 20 ]; then
	echo "FAIL"
#	echo "MISMATCH OF SOLVER RETURN VALUE AND STANDARD INPUT"
	exit 1
fi
if [ "$answerRow" != "INCOMPLETE" ] && [ $1 != 10 ] && [ $1 != 20 ]; then
	echo "FAIL"
#	echo "MISMATCH OF SOLVER RETURN VALUE AND STANDARD INPUT"
	exit 1
fi

if [ $1 == 20 ]; then
	echo "FAIL"
#        echo "Solver reported inconsistency. However, all Still Live instances are satisfiable." >&2
        exit 1
else
	if [ $1 != 10 ]; then
		echo "FAIL"
	        exit 1
	else
		systemResult=$((echo "$answerRow" ; cat $2 checker.gringo) | ./clingo --quiet)
		systemReturn=$?

		if [ $systemReturn == 20 ]; then
    			echo "FAIL"
#        		echo "Solver reported incorrect witness." >&2
    			exit 1
		else
			if [ $systemReturn != 10 ]; then
				echo "WARN"
    				echo "Unrecognized input stream or general checker failure." >&2
    				exit 1
			fi
		fi
	fi
fi

echo "OK"

