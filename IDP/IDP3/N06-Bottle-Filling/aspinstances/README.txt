instance.asp is the instance depicted in the newspaper article. When allowing
empty bottles, it has three solutions (output.instance.1.txt to
output.instance.3.txt).

simplest.asp and simple1.asp are very simple instances that have unique
solutions (output.simple.txt and output.simple1.txt, respectively).

noncomm.unsat.asp is an unsatisfiable instance that illustrates the issue of
bottle cells that are not commuting. Using standard physics, this would have
a solution in which one area of the bottle has a diferent fill level than
another, which is possible in the real world because the areas are not
commuting. In the world considered in the benchmark, however, this is not
possible and all areas must have the same fill level, even if they are not
commuting, hence this instance is unsatisfiable.

noncomm.sat.asp is a variant of noncomm.unsat.asp which is satisfiable, as the
noncommuting areas need to have the same fill level (output.noncomm.sat.txt).

nonconvex.asp is an example of a nonconvex bottle (completely surrounding an
area that is not part of the bottle), solution in output.nonconvex.txt.

The directory larger-instances contains some harder input for testing.

