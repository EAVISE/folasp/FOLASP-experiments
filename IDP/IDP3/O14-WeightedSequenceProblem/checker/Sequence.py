#!/usr/bin/python

class Sequence:
    
    def __init__(self):
        # Represent nodes with a dictionary. These may not be read in sequence,
        # so it is easiest to just hash by their index instead of using a list.
        self.leaves = {} 

        # each leaf's position in our sequence
        self.leafPos = {}

        # each node's individual cost
        self.leafCost = {}

        # Total cost of a sequence
        self.cost = 0

        # maxWeight specified by problem
        self.maxCost = -1

        self.posColor = {}
    

class Node:
    
    def __init__(self, weight, card):
        """ A structure used to store color, cost, and weight"""
        self.c = card
        self.w = weight

