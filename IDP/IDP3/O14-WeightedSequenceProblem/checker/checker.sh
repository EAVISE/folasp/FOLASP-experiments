#!/bin/bash

ok=false
EXIT_CODE=$1
INSTANCE_FILE=$2

read answerRow

if [ "$answerRow" == "INCONSISTENT" ] && [ $1 != 20 ]; then
	echo "FAIL"
	echo "MISMATCH OF SOLVER RETURN VALUE AND STANDARD INPUT" >&2
	exit 1
fi
if [ "$answerRow" != "INCOMPLETE" ] && [ $1 != 10 ] && [ $1 != 20 ]; then
	echo "FAIL"
	echo "MISMATCH OF SOLVER RETURN VALUE AND STANDARD INPUT" >&2
	exit 1
fi

if [[ "$1" == 20 ]]; then
    if [[ "$answerRow" == "INCONSISTENT" ]]; then
        echo "DONTKNOW"
        echo "Instances with no solution should not be checked with this checker" >&2
        exit 2
    else # exit code 20, something other than INCONSISTENT
        echo "FAIL"
        echo "MISMATCH OF SOLVER RETURN VALUE AND STANDARD INPUT" >&2
        exit 1
    fi
else
	if [ $1 != 10 ]; then
		echo "FAIL"
        exit 1
	else
		systemResult=$(echo "$answerRow" | python checker.py ${EXIT_CODE} ${INSTANCE_FILE})
		if [[ $? -eq 0 ]]; then
            ok=true
        elif [[ $? -eq 1 ]]; then
            ok=false
        else
            echo "WARN"
            echo "Unrecognized input stream or general checker failure." >&2
            exit 1
		fi

	fi
fi
if $ok; then
	echo "OK"
    exit 0
else
	echo "FAIL"
    exit 1
fi 

