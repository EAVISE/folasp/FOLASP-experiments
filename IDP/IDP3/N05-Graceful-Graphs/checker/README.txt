To use the checker, execute execute it in a terminal console with two parameters,
the exit code from the solver and the path to the problem instance, and giving,
in standard input, a witness as a set of facts on a single line. The checker will assess
whether this represents a graceful labelling or not.

Requires clingo (tested with version 3.0.4). http://potassco.sourceforge.net

For example, to test a correct and an incorrect solution, use the following commands:

	./checker.sh 10 ../example/instance.asp < rightSolution.txt
	./checker.sh 10 ../example/instance.asp < wrongSolution.txt

