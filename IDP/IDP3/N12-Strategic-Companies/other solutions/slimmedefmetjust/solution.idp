include "processing.idp"

vocabulary inputTypes{
	type Company
  	type Good

}
vocabulary input {
	extern vocabulary inputTypes
	controlled_by(Company,Company,Company,Company,Company)
	produced_by(Good,Company,Company,Company,Company)
	non_strategic_pair(Company,Company)
}

vocabulary translatedInput{
	extern vocabulary inputTypes
	produces(Company,Good)
	controls(Company,Company)
	uncontrolled(Company)
	C1:Company
	C2:Company
}

vocabulary translation{
	extern vocabulary input
	extern vocabulary translatedInput
}

theory transTheo:translation{
	{
		produces(x,g) <- produced_by(g,x,y,z,u).
		produces(y,g) <- produced_by(g,x,y,z,u).
		produces(z,g) <- produced_by(g,x,y,z,u).
		produces(u,g) <- produced_by(g,x,y,z,u).
	}
	{
		controls(x,g) <- controlled_by(g,x,y,z,u).
		controls(y,g) <- controlled_by(g,x,y,z,u).
		controls(z,g) <- controlled_by(g,x,y,z,u).
		controls(u,g) <- controlled_by(g,x,y,z,u).
	}
	{
		uncontrolled(c) <- ~? x: controls(x,c).
	}
	{
		C1=s <- 	non_strategic_pair(s,t).
		C2=t <- 	non_strategic_pair(s,t).
	}
	

}

vocabulary candidatesVoc{
	extern vocabulary translatedInput
	inCandidate(Company)
	ownedByCandidate(Company)
	uniqueProducer(Company)
}


theory candidateTheo : candidatesVoc{
	uniqueProducer(x) <=> inCandidate(x) & ? g: produces(x,g) & ~? y: y~= x & produces(y,g) & inCandidate(y).
	
	inCandidate(C1) & inCandidate(C2).
	!p: ? c: inCandidate(c) & produces(c,p).
	{
		ownedByCandidate(c) <- ~uncontrolled(c) & (! o: controls(o,c) => inCandidate(o)).
	}
	ownedByCandidate(c) => inCandidate(c).
	inCandidate(c) => ownedByCandidate(c) | uniqueProducer(c).
}

vocabulary checkCandidateVoc{
	extern vocabulary candidatesVoc
    inSubset(Company) 
    ownedBySubset(Company)
}

theory checking: checkCandidateVoc{
	inSubset(c) => inCandidate(c). //subset relation
	? c: ~inSubset(c) & inCandidate(c). //Not equal
	!p: ? c: inSubset(c) & produces(c,p).
	{
		ownedBySubset(c) <- ~uncontrolled(c) & (! o: controls(o,c) => ownedBySubset(o)).
	}
	ownedBySubset(c) => inSubset(c).
}
