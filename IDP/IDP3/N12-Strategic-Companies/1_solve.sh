#!/bin/bash
# REMEMBER: first argument is a string representing a name for this run.
## timelimit: 10000
## memlimit: 5000
## output: |  | .out
## linked_input: |  | .dat
#idp solution.idp $2
/cw/dtailocal/bartb/local/bin/idp --nowarnings solution.idp $2
