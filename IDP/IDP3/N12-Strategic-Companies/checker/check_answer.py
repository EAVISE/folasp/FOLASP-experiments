#!/usr/bin/env python

import sys

ok = False
providedResult = sys.argv[1]
instance = int(sys.argv[2].partition("-")[0])

expectedResult = "no."
if instance <= 25:
    expectedResult = "yes."

if instance < 1 or instance > 50:
    print "WARN"
    print "I cannot check this instance."
else:
    if providedResult == expectedResult:
        ok = True
    else:
        ok = False

if ok:
    print "OK"
    sys.exit(0)
else:
    print "FAIL"
    sys.exit(1)
