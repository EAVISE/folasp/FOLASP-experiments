import sys, subprocess, hashlib, base64
from concurrent.futures import ThreadPoolExecutor
from time import sleep
from collections import namedtuple
import os

def getHash(instance):
    return str(base64.b64encode(hashlib.md5(instance.encode()).digest()))[2:-3].replace('/','-')

def runInstance(step, instance):
    purehash = getHash(instance)
    metafile = step.outputdir+'/'+ purehash + ".meta"
    if not os.path.exists(metafile):
        print("running",instance,"with",step.timelim,"timeout and",step.memlim,"memout")
        subprocess.run(["python3", "runlimited.py", step.timelim,step.memlim, step.outputdir, purehash]+instance.split())
    else: 
        print("skipping",instance)

def getCSVline(step,instance):
    corefile = step.outputdir+'/'+getHash(instance)
    metafile = corefile + ".meta"
    outfile = corefile + ".out"
    result = ""
    with open(metafile, 'r') as meta:
        for line in meta.readlines():
            if not line.startswith("## "): continue
            result += line.split()[2] + "; "
    result+=str(subprocess.check_output(["bash",step.parser,outfile]),"utf-8")
    while result.endswith("\n"):
        result = result[:-1]
    return result

def runStep(step):
    print("Running step",step.runs)
    instances = []
    for line in str(subprocess.check_output(step.runs),"utf-8").split("\n"):
        line = line.strip()
        if line=="": continue
        instances.append(line)
    with ThreadPoolExecutor(max_workers=step.threads) as executor:
        for instance in instances:
            executor.submit(runInstance,step,instance)
    sleep(1)
    
    print("Constructing csv for",step.runs)
    csvfile=step.runs+".csv"
    with open(csvfile, 'w') as csv:
        csv.write("sha; outcode; user time; system time; memory; ")
        result = str(subprocess.check_output(["bash",step.csvheader]),"utf-8")
        while result.endswith("\n"):
            result = result[:-1]
        csv.write(result+"\n")
        for instance in instances:
            csv.write(getCSVline(step,instance)+ "\n")

Step = namedtuple("Step","runs, parser, csvheader, outputdir, timelim, memlim, threads")
steps = []

# MAIN:

if len(sys.argv)<2 or len(sys.argv)>3:
    print("Incorrect number of arguments\nUsage: python3 Benchie3.py <config-file> <append steps>")

configfile = open(sys.argv[1],'r')
append = len(sys.argv)>2 and sys.argv[2]=="append" # TODO use in implementation?
for line in configfile.readlines():
    line=line.strip()
    if line.startswith("#") or line=="": continue
    lspl = line.split()
    steps.append(Step(lspl[0],lspl[1],lspl[2],lspl[3],lspl[4],lspl[5],int(lspl[6])))
    print(steps[-1])
configfile.close()
    
for step in steps:
    runStep(step)
